<?php

//checks username and password in database
function validateUser($con,$uname,$pass){
	$params = array($uname,$pass);
	$query = "SELECT * FROM validate($1,$2);";
	$query_result = pg_query_params($con,$query,$params);
	
	if(!$query_result){
		echo "Query error\n";
		echo pg_last_error();
	}
	
	$row = pg_fetch_row($query_result);
	if($row[0] == "1"){
		return TRUE;
	} else {
		return FALSE;
	}
}

// initialise an array of 0's
function initArray($size){
	$a = array();
	for($i = 0; $i < $size; $i++){
		$a[$i] = 0;
	}
	
	return $a;
}

// divides array a, by a number d
function divideArray($a,$d){
	$a2 = array();
	for($i = 0; $i < count($a); $i++){
		$a2[$i] = $a[$i]/$d;
	}
	return $a2;
}

// adds a1 and a2, returns new array
function addArray($a1,$a2){
	$a3 = array();
	for($i = 0; $i < count($a2); $i++){
		$a3[$i] = $a1[$i] + $a2[$i];
	}
	
	return $a3;
}

// gets the table name for vehicle with vid
function getTableName($con, $vid){
	$query = "SELECT * FROM getTableName($1)";
	$params = array($vid);
	$query_result = pg_query_params($con,$query,$params);
	
	return pg_fetch_result($query_result, 0, 0);
	
}

// generates the query string to retrieve the data values 
// between start and end dates. Ascending order.
function rawDataQueryBetween($columns, $table_name, $start, $end){
	$query = "SELECT";

	for($i = 0; $i < count($columns); $i++){
		$query .=" ".$columns[$i];
		if($i != count($columns) - 1) // if not last
			$query .= ",";
	}
	
	$query .= " FROM ".$table_name." WHERE systime > ".$start." AND systime < ".$end." ORDER BY systime asc".";";
	return $query;
}

// generates query to retreive all data values in table
// Ascending order.
function rawDataQuery($columns, $table_name){
	$query = "SELECT";

	for($i = 0; $i < count($columns); $i++){
		$query .=" ".$columns[$i];
		if($i != count($columns) - 1) // if not last
			$query .= ",";
	}
	
	$query .= " FROM ".$table_name." ORDER BY systime asc".";";
	
	return $query;
}

// generates query to retrieve just the last entry since last_time.
// The newest entry in the table.
function rawDataQueryLast($columns,$table_name,$last_time){
		$query = "SELECT";

	for($i = 0; $i < count($columns); $i++){
		$query .=" ".$columns[$i];
		if($i != count($columns) - 1) // if not last
			$query .= ",";
	}
	
	$query .= " FROM ".$table_name." WHERE systime > " .$last_time." ORDER BY systime desc limit 1".";";
	
	return $query;
}

//returns an array of column names
function getColumn($con,$vid,$col_index){
	$query = "SELECT * FROM getDescription($1);";
	$params = array($vid);
	$query_result = pg_query_params($con,$query,$params);
	
	$col = array();
	
	while ($row = pg_fetch_array($query_result)) {
		$col[] = ($row[$col_index]);	
	}
	
	return $col;
		
}

function getRangeQuery($table_name){
	$query = "(SELECT min(systime) as systime FROM ".$table_name.") ";
	$query .= "UNION ";
	$query .= "(SELECT max(systime) FROM ".$table_name.");";
	
	return $query;
}

// convert single row to a csv
function rowToCSV($row){
	$first = true;	
	$csv="";	
	foreach($row as $val){
		if(!$first){
			$csv = $csv.",";
		}
		$csv = $csv.$val;
		$first = false;
	}
	return $csv;
}

// convert dataset to csv
function queryToCSV($query_result){
	$first = true;
	$csv="";
	while ($row = pg_fetch_row($query_result)) {
		if(!$first)
			$csv = $csv."\n";	
		$csv = $csv.rowToCSV($row);
		$first = false;
	}
	
	return $csv;
}

// helper function to visualise array
function printArray($array){
	echo "Elements: (". count($array).")\n";
	echo "Contents\n[";
	foreach ($array as $a){
		echo $a.", ";
	}
	echo "]\n";
}

function getMatchedKeyIndexes($pattern, $array){
	$i = 0;
	$index = array();
	foreach ($array as $key => $value){
		if(preg_match($pattern, $key) == 1){
			// must divide index by two because associative arrays
			// have two entries for each element
			$index[] = floor($i/2);
		}
		$i++;
	}
	
	return $index;
}

?>