<?php
$base = dirname(__FILE__);
include $base."/../lib/utils.php";
include $base."/../lib/db.php";
include $base."/../lib/convert.php";


$DESCRIPTION_COL_NAME_IDX = 0;
$debug = FALSE;

$maxPoints = null;
if(!$debug){
	$zone = $_GET["zone"];
	$last = $_GET["last"];
	str_replace('"', "", $last);
	$convert = $_GET["convert"];
	$vid = $_GET["vid"];
	$maxPoints = $_GET["max"];
} else { // These should be passed in from the calling code
	echo "PHP Started\n";
	$convert = true;
	$zone = 56;
	$vid = 1;
	$last = "1.37044225835E+12";
	//$last = 2070853281.98;
	//$maxPoints = 100;
}


// connect to database
$con = connect();
// get name of table
$table_name = getTableName($con,$vid);
// get description in order
$columns = getColumn($con,$vid,$DESCRIPTION_COL_NAME_IDX);

// use column names in order to retrieve raw data
$query = rawDataQueryLast($columns,$table_name,$last);

$query_result = pg_query($con,$query);
if($debug)
	echo $query."\n";
// retrieve data


if(!$query_result){
	echo "Query error\n";
	echo pg_last_error();
}

$first = true;

// if conversion requested
if($convert){
	while ($row = pg_fetch_row($query_result)) {

			
			// Append lat/long to end of array
			$easting = $row[1];
			$northing = $row[0];
			$point = redfearnGridtoLL($easting,$northing,$zone);
			$row[count($row)] = $point['Latitude'];
			$row[count($row)] = $point['Longitude'];
			// Output to stream
			if(!$first){
				echo "\n";
			}
			$first = false;
			echo rowToCSV($row);

		
	}

}

if($row_count > 0){
	echo"\n";
	$avg = divideArray($sum,$row_count);

	$easting = $avg[1];
	$northing = $avg[0];
	
	$point = redfearnGridtoLL($easting,$northing,$zone);
	$avg[count($avg)] = $point['Latitude'];

	$avg[count($avg)] = $point['Longitude'];
	
	echo rowToCSV($avg);
}

if($debug){
	//echo "\nPHP ended";
}
?>