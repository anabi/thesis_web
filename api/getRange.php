<?php
$base = dirname(__FILE__);
include $base."/../lib/utils.php";
include $base."/../lib/db.php";

$DESCRIPTION_COL_NAME_IDX = 0;
$debug = FALSE;
if(!$debug){
	$vid = $_GET["vid"];
} else {
	$vid = 1;
}

// connect to database
$con = connect();

// get name of table
$table_name = getTableName($con,$vid);

$query = getRangeQuery($table_name);

$query_result = pg_query($con,$query);

// retrieve data
if($debug) echo $query."\n";

if(!$query_result){
	echo "Query error\n";
	echo pg_last_error();
}


while ($row = pg_fetch_row($query_result)) {
	echo $row[0].",".$row[1];
}

if($debug){
	echo "\nPHP ended";
}
?>