<?php
$base = dirname(__FILE__);
include $base."/../lib/utils.php";
include $base."/../lib/db.php";
include $base."/../convert.php";


$debug = false;

if(!$debug){
	$vid = $_GET["vid"];
} else {
	$vid = 1;
}
// connect to database
$con = connect();

$query = "SELECT * FROM getDescription($1);";
$params = array($vid);

// retrieve data
$query_result = pg_query_params($con,$query,$params);
if(!$query_result){
	echo "Query error\n";
	echo pg_last_error();
}

// convert to csv and send
echo queryToCSV($query_result);

pg_close($con);
?>