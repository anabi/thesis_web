<?php

$base = dirname(__FILE__);
include $base."/../lib/utils.php";
include $base."/../lib/db.php";

// connect to database
$con = connect();

$query = 'SELECT * FROM getVehicles();';

// retrieve data
$query_result = pg_query($con,$query);

if(!$query_result){
	echo "Query error\n";
	echo pg_last_error();
}

echo queryToCSV($query_result);

?>