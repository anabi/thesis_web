<?php
$base = dirname(__FILE__);
include $base."/../lib/utils.php";
include $base."/../lib/db.php";

$uname = $_POST["user"];
$pass = $_POST["pwd"];

$con = connect();

// check username and password
if(!validateUser($con,$uname,$pass)){
	echo "Invalid username or password!";
	return;
}

switch($_POST['submit']){
	case 'add': // return the add vehicles page, embed hidden password
		$file = file_get_contents("../add_vehicle.html");
		$file = str_replace("@user", $uname, $file);
		$file = str_replace("@pass", $pass, $file);
		echo $file;
		break;
	case 'create': // return the create type page, embed hidden password
		$file = file_get_contents("../create_type.html");
		$file = str_replace("@user", $uname, $file);
		$file = str_replace("@pass", $pass, $file);
		echo $file;
	default:
		break;
}

if($_POST['type_name'] != null){
	handleCreate($con);	
} else if ($_POST['vname'] != null){
	handleAdd($con);
}

function handleAdd($con){
	$vname = $_POST['vname'];
	$tid = $_POST['tid'];
	
	// use typeid to get desctiption
	$query = "SELECT * FROM getDescriptionTID(".$tid.");";
	
	$result = pg_query($con,$query);
	
	if(!$result){
		echo "Query error\n";
		echo pg_last_error();
	}
	
	$numRows = pg_num_rows($result);
	$rowCount = 0;
	$colnames = initArray($numRows);
	$datatypes = initArray($numRows);
	while ($row = pg_fetch_row($result)){
		$colnames[$rowCount] = $row[0];
		$datatypes[$rowCount] = $row[1];
		$rowCount++;
	}

	// create sql string to create the table with name_type	
	$table_name = $vname."_".$tid;
	$query = "CREATE TABLE ".$table_name." (";
	
	for($i = 0; $i < $numRows; $i++){
		$query.= $colnames[$i]." ".$datatypes[$i];
		if($i != ($numRows - 1)){ // not last element
			$query.=", ";
		}
	}
	$query.=");";
	
	$result = pg_query($con,$query);
	
	if(!$result){
		echo "Query error\n";
		echo pg_last_error();
	} else {
		echo "Success creating table";
	}	
	
	// insert name_type as table name and other info in to master lut
	$query = "INSERT INTO MasterLUT(\"name\",typeid,\"table\") ";
	$query.= "VALUES($1,$2,$3);";
	$params = array($vname,$tid,$table_name);
	$result = pg_query_params($con,$query,$params);
	
	if(!$result){
		echo "Query error\n";
		echo pg_last_error();
	} else {
		echo "\nSuccess updating lookup";
	}	
	
}



function handleCreate($con){
	$postCount = count($_POST);

	$numRows = ($postCount-1)/4;

	$typeName = $_POST["type_name"];

	for($i = 1; $i <= $numRows; $i++){
		$name = $_POST[$i.",1"];
		$colname = $_POST[$i.",2"];
		$units = $_POST[$i.",3"];
		$datatype = $_POST[$i.",4"];
		
		$params = array($typeName,$i,$name,$colname,$units,$datatype);
		
		$query = "SELECT insertDescription($1,$2,$3,$4,$5,$6);";
		$result = pg_query_params($con,$query,$params);
	}
	
	echo "Success";
	
}
?>