<?php
$base = dirname(__FILE__);
include $base."/../lib/utils.php";
include $base."/../lib/db.php";
include $base."/../convert.php";

$DESCRIPTION_COL_NAME_IDX = 0;
$debug = FALSE;

$maxPoints = null;
if(!$debug){
	$zone = $_GET["zone"];
	$last = $_GET["last"];
	$convert = $_GET["convert"];
	$vid = $_GET["vid"];
	$maxPoints = $_GET["max"];
	$start = $_GET["start"];
	$end = $_GET["end"];
} else { // These should be passed in from the calling code
	echo "PHP Started\n";
	$convert = true;
	$zone = 56;
	$vid = 1;
	$maxPoints = 100;
	$start = "1370442946088";
	$end = "1370442949148";//"1370443968844";
}


// connect to database
$con = connect();
// get name of table
$table_name = getTableName($con,$vid);
// get description in order
$columns = getColumn($con,$vid,$DESCRIPTION_COL_NAME_IDX);

// use column names in order to retrieve raw data
if($start != null && $end != null){ // return data between start and end
	$query = rawDataQueryBetween($columns,$table_name,$start,$end);
} else { // return all data
	$query = rawDataQuery($columns,$table_name);
}
$query_result = pg_query($con,$query);

if(!$query_result){
	echo "Query error\n";
	echo pg_last_error();
}

if(is_null($maxPoints) || $maxPoints == '0' || $maxPoints >= pg_num_rows($query_result)){
	$decimate_count = 1;
} else {
	// how many points to average
	$decimate_count = floor(pg_num_rows($query_result)/$maxPoints);
}

//echo $decimate_count;
$row_count = 0;

$first = true;
$num_fields = pg_num_fields($query_result);
$sum = initArray($num_fields);
// if conversion requested


$return_string = rowToCSV($columns)."\n";
if($convert){
	while ($row = pg_fetch_row($query_result)) {
		
		// decimate the data
		if($row_count == $decimate_count){
			
			$avg = divideArray($sum,$row_count);
			
			// Append lat/long to end of array
			$easting = $avg[1];
			$northing = $avg[0];
			$point = redfearnGridtoLL($easting,$northing,$zone);
			$avg[count($avg)] = $point['Latitude'];
			$avg[count($avg)] = $point['Longitude'];
			
			// Output to stream
			if(!$first){
				$return_string.= "\n";
			}
			$first = false;
			$return_string.= rowToCSV($avg);
			
			// reset array and row count
			$row_count = 0;
			$sum = initArray($num_fields);
		}
		
		$sum = addArray($sum,$row);
		$row_count++;
		//return;
		
	}
	// find easting/northings
	// convert data
}

if($row_count > 0){
	$return_string.="\n";
	$avg = divideArray($sum,$row_count);
			
	//var_dump($avg);
	//return;
	$easting = $avg[1];
	$northing = $avg[0];
	
	$point = redfearnGridtoLL($easting,$northing,$zone);
	//echo count($row)."\n";
	$avg[count($avg)] = $point['Latitude'];
	//echo count($row)."\n";
	$avg[count($avg)] = $point['Longitude'];
	
	$return_string.= rowToCSV($avg);
}

// send to client
header("Content-Length: " . strlen($return_string));
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='."file.csv");
echo $return_string;

if($debug){
	echo "\nPHP ended";
}
	
?>