var map;
var markerArray = [];
var infoWindow;

function initialiseMap(){
	var mapOptions = {
	  center: new google.maps.LatLng(-34.397, 150.644),
	  zoom: 8,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);
	
	infoWindow = new google.maps.InfoWindow();

        
}
function clearMarkers(){
    markerArray.length = 0;
}

var opacity = 0.4;

// Info window trigger function 
function onItemClick(event, pin, info) { 
  	// Create content  
  	var contentString ="hello this is some sort of large content\nstring can you see it?"; 

  	// Replace our Info Window's content and position 
  	infoWindow.setContent(info); 
  	infoWindow.setPosition(pin.position); 
  	infoWindow.open(map) 
} 


function addMarker(location,info){
    
    var smallSquareMarker = {
        path: 'M 2,2 -2,2 -2,-2 2,-2 z',
	    scale: 1,
        strokeColor: "red",
        strokeWeight: 4,
        strokeOpacity: 0.8
    }
    
    var marker = new google.maps.Marker({
		position: location,
		title:  location.toString(),
		icon: smallSquareMarker,
		map: map
	});
	
	google.maps.event.addListener(marker, 'click', function() { 
	    map.setCenter(new google.maps.LatLng(marker.position.lat(), marker.position.lng())); 
	    map.setZoom(25); 
	    onItemClick(event, marker,info); 
  }); 
}

function setCenter(position){
    map.setCenter(position);
    map.setZoom(16);
}

