var rowCount = 3;
var colCount = 1;

function addRow(){
	$('#table tbody>tr:last').clone(true).insertAfter('#table tbody>tr:last');
	$('#table tbody>tr:last').each(function(){
			colCount = 1; // reset column counter
			rowCount++;
			
			$(this).find('td').each(function(){
			$(this).children().first().attr('name',(rowCount)+","+(colCount++));
		});
	});           
}

// Deletes the last row
function deleteRow(){
	$('#table tbody>tr:last').remove();
}
