
var chartData;
var chartOptions;
function drawCharts(speed,eng_temp,fuel){
	var hh = $('#gauges_div').height;
	var ww = 300; 
	chartOptions = {
	 width: 250, height: 100,
	  redFrom: 90, redTo: 100,
	  yellowFrom:75, yellowTo: 90,
	  minorTicks: 5
	};
	
	$('#speed_div').width(0.95*ww);

	$('#speed_div').height(0.95*hh);
	var array = [];
	array.push(['Label','Value']);
	if(eng_temp != UNASSIGNED && typeof(eng_temp) != 'undefined')
		array.push(['Eng Temp',eng_temp]);
	if(speed != UNASSIGNED && typeof(speed) != 'undefined')
		array.push(['Speed',speed]);
	if(fuel != UNASSIGNED && typeof(fuel) != 'undefined')
		array.push(['Fuel',fuel]);
	chartData = google.visualization.arrayToDataTable(array);
	
	var num_gauges = array.length-1;
	if(num_gauges > 0){
		var chart = new google.visualization.Gauge(document.getElementById('speed_div'));

		chart.draw(chartData, chartOptions);
	}

}

function updateChart(speed,eng_temp,fuel){
	var index = 0;
	if(eng_temp != UNASSIGNED && typeof(eng_temp) != 'undefined'){
		chartData.setValue(index++,1,parseFloat(eng_temp));
	}
	if(speed != UNASSIGNED && typeof(speed) != 'undefined'){
		chartData.setValue(index++,1,parseFloat(speed));
	}
	if(fuel != UNASSIGNED && typeof(fuel) != 'undefined'){
		chartData.setValue(index++,1,parseFloat(fuel));
	}
	if(index > 0){
		var chart = new google.visualization.Gauge(document.getElementById('speed_div'));
		chart.draw(chartData, chartOptions);
	}
}