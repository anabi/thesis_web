
function enableAll(){
	enableButton('#plot_button');
	enableButton('#live_button');
	enableButton('#playback_button');
}

function disableAll(){
	disableButton('#plot_button');
	disableButton('#live_button');
	disableButton('#playback_button');
}

function disableButton(button){
	$(button).fadeTo(300,0.5);
	$(button).attr("disabled", true); // enable plot

}

function enableButton(button){
	$(button).fadeTo(300,1.0);
	$(button).attr("disabled", false); // enable plot

}

function plotAllData(gpsdata){
	
	var next = gpsdata.getNextPlayback();
	var nextLocation;
	var nextInfo;
	//var nextLocation = gpsdata.getNextPlayback();
	while(next != null){
	    // set the first element in array as center
	    // only do once per playback
	    nextLocation = next['loc'];
		nextInfo = next['info'];
		//var nextLocation = gpsdata.g
	    if(gpsdata.currentPlaybackIndex() == 2){
	       setCenter(nextLocation);
	    }
	 
        addMarker(nextLocation,nextInfo);
    	next = gpsdata.getNextPlayback();
        //nextLocation = gpsdata.getNextPlayback();
    }
    //showOverlay(markerArray);
    
    hideMenu();    
    hideLoading();
}

function createGPSData(coords){
		for(i in coords){
			var latLong = coords[i].split(",");
            // reverse the order of the index of latLong because in reverse
            // order to my test data
            gpsdata.add(new google.maps.LatLng(latLong[1], latLong[0]));
		}
   return gpsdata; 
}



function updateVehicleSelector(){
	$('#vehicle_select').find('option').remove().end(); // clear select element
	// update list with data in model
	$("#vehicle_select").append("<option value='" + -1 + "'>" + "Select Vehicle" + "</option>");
	var vList = model.getVehicleList();
	for(var i = 0; i < vList.length; i++){
		$("#vehicle_select").append("<option value='" + vList[i].id + "'>" + vList[i].name + "</option>");
	}	

}



function appendText(dom,msg){
	var text = dom.text();
	dom.text(text + "\n" + msg);
}

function toggleMenu(){
	if($("#menu_div").css('z-index') == ZBACK){
		showMenu();
	} else {
		hideMenu();
	}
}

function hideMenu(){
	$("#map_canvas").css('z-index',ZFRONT);
	$("#gauges_div").css('z-index',ZFRONT);
	$("#menu_div").css('z-index',ZBACK);
}

function showMenu(){
	$("#map_canvas").css('z-index',ZBACK);
	$("#gauges_div").css('z-index',ZBACK);
	$("#menu_div").css('z-index',ZFRONT);
}

function toggleLoading(){
	if($("#outer").css('z-index') == ZBACK){
		showLoading();
	} else {
		hideLoading();
	}
}

function showLoading(){
	var width = $(window).width();
	var height = $(window).height();
	
	$("#outer").css('margin-top',(height/2 - 50)+'px') // vertically align
	
	$("#outer").css('z-index',ZLOADING);
	$("#middle").css('z-index',ZLOADING);
	$("#inner").css('z-index',ZLOADING);
}

function hideLoading(){
	$("#outer").css('z-index',ZBACK);
	$("#middle").css('z-index',ZBACK);
	$("#inner").css('z-index',ZBACK);
}



