function GPSData(){
    // Use setInterval for playback
    this.array = []; 		// store LatLng values
    this.lastIndex = 0; 	// store size of array
    						// do i need this? array.length
    this.playbackIndex = 0;
    
    this.info = []; 		// array of strings to display in the markers
}							// info window

GPSData.prototype.currentPlaybackIndex = function(){
    return this.playbackIndex;
}

GPSData.prototype.add = function(latLong, pointInfo){
    this.array.push(latLong);
    this.info.push(pointInfo);
    this.lastIndex++;    
}

GPSData.prototype.getNextPlayback = function(){
    if(this.playbackIndex >= this.array.length){
        return null; 		// return null when finished
                    		// find something better to return
    }
    var result = {loc: this.array[this.playbackIndex],info: this.info[this.playbackIndex]};
    this.playbackIndex++;
    return result;
}

GPSData.prototype.resetPlayback = function() {
    this.playbackIndex = 0;
}

GPSData.prototype.clearData = function(){
    // deletes all elements in array
    this.array = 0;
    this.lastIndex = 0;
    this.playbackIndex = 0;
}

