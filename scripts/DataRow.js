var TIME_INDEX = 7;

function DataRow(array){
	this.fields = array;
}

DataRow.prototype.getField = function(idx){
	return this.fields[idx];
}

DataRow.prototype.getLength = function(){
	return this.fields.length;
}

// Last two fields are lat/long pairs
DataRow.prototype.getLat = function(){
	return this.fields[this.fields.length - 2];
}

DataRow.prototype.getLong = function(){
	return this.fields[this.fields.length - 1];
}