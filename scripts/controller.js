var MAP_WIDTH = 1;
var MAP_HEIGHT = 0.85;
var MENU_WIDTH = 1;
var MENU_HEIGHT = 0.04;
var INTERVAL_DELAY = 3000; //ms

var ZFRONT = 10;
var ZBACK = 1;
var ZLOADING = 999;

var POINTS_AVG = 40;
var MAX_POINTS = 100;

var intervalHandler;

var DESC_COLNAME_IDX = 0;
var DESC_DTYPE_IDX = 1;
var DESC_NAME_IDX = 2;
var DESC_UNITS_IDX = 3;

var INVALID_OPTION = -1;

var model = new Model();
var options = new Options();

var isLive = false;
var isPlayback = false;
var isPlot = false;
function live_clicked(){
	isLive = true;
	plot_clicked(); // plot existing data
	//must wait for this to complete before firing off the interval ticks
}
// asynchronous request to get next data point
function liveDataUpdate(){
	var lastTime = this.model.getLastTime();
	var selectedVID = this.model.selectedVID;
	var request = new XMLHttpRequest();
	lastTime = lastTime.replace("+","%2B") // url encoding the + in xxxE+Y
	request.open("GET","api/getLive.php?vid="+selectedVID+"&zone=56&convert=true&max="+1+"&last="+lastTime,false);
	request.send();
	liveUpdate_stateChanged(request);
}
// next data point received
function liveUpdate_stateChanged(response){
	if(response.readyState == 4 && response.status==200){
        var responseText = response.responseText;
		if(responseText.indexOf("Query error") == -1){
			var dataArray = responseText.split(",");
			if(dataArray.length > 1){
				this.model.addDataRow(dataArray); // add raw data
				this.model.createLastGPSData();  // create gps point for it
				var gpsData = this.model.getGPSData();
			}
		}
	}
}

// update view if there is new playback data
function liveViewUpdate(){
	var gpsdata = this.model.getGPSData();
	var next = gpsdata.getNextPlayback(); 
	if(next != null){
		var nextLocation = next['loc'];
		var info = next['info'];
		if(gpsdata.currentPlaybackIndex() == 1){
       		setCenter(nextLocation);
    	}
    	addMarker(nextLocation,info);
    	        
		if(isLive){ // if live update gauges
 			vehicle = model.getSelectedVehicle();
		
			var speed = vehicle.getSpeed();
			var eng_temp = vehicle.getEngTemp();
			var fuel = vehicle.getFuel();
		 	
		 	updateChart(speed,eng_temp,fuel);
		}

	}
	
}
// update list of vehicles, fired off on startup
function connect_clicked(){
	var request = new XMLHttpRequest();
	request.onreadystatechange = function(){
		vehicleList_stateChanged(request);	
	}	
	request.open("GET","api/getVehicles.php")
	request.send();
}
// add list of vehicles to select control and update
function vehicleList_stateChanged(response){
    if(response.readyState == 4 && response.status==200){
		var rows = response.responseText.split("\n");
		for(var i = 0; i < rows.length; i++){
			var values = rows[i].split(",");
			this.model.addVehicle(values[0],values[1],values[2],values[3]);
		}
		updateVehicleSelector();
	}	
}

function plot_clicked(){
	isPlot = true;
	showLoading();
    clearMarkers();
    setOptions();
    var selectedVID = $("#vehicle_select option:selected").val();
    this.model.selectedVID = selectedVID;
	var request = new XMLHttpRequest();
	request.onreadystatechange=function(){
		plot_stateChanged(request);
	}
	var maxPoints = this.options.maxPoints;
	
	var startString = $('#startDate_picker').val();
	var endString = $('#endDate_picker').val();
	// convert to date epoch ms
	// start time
	var startParts = startString.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/);
	if(startParts != null){
		var startDate = new Date(+startParts[3], startParts[2]-1, +startParts[1], +startParts[4], +startParts[5]);
		var start = startDate.valueOf();
	}
	// end time
	var endParts = endString.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/);
	if(endParts != null){
		var endDate = new Date(+endParts[3], endParts[2]-1, +endParts[1], +endParts[4], +endParts[5]);
		var end = endDate.valueOf();
	}
	
	if(endParts == null || startParts == null){ // get all data if no time range is specified or invalid
		request.open("GET","api/getData.php?vid="+selectedVID+"&zone=56&convert=true&max="+maxPoints,true);
	} else { // get data between specified time ranges
		request.open("GET","api/getData.php?vid="+selectedVID+"&zone=56&convert=true&max="+maxPoints+"&start="+start+"&end="+end,true);
	}
	request.send();
}
// advanced options
function setOptions(){
	var maxPoints = $('input[id=maxPoints_input]').val();
	this.options.maxPoints = maxPoints;
	var interval = $('input[id=interval_input]').val();
	this.options.interval = parseFloat(interval);
}

// get the data for the vehciles descriptions
// this is needed to make sense of the raw data returned in csv file
function getDescriptionData(vid)
{
	if(!this.model.descInitialised(vid)){
		var request = new XMLHttpRequest();
		request.open("GET","api/getDescription.php?vid="+vid,false);
		request.send();
		
		if(request.readyState == 4 && request.status==200){
			description_stateChanged(request);
		}
	} else {
		hideLoading();
		enableAll();
	}
}

function description_stateChanged(response){
	if(response.readyState == 4 && response.status==200){
		var responseText = response.responseText;
		var rows = responseText.split("\n");
		for(var i in rows){
			cols = rows[i].split(",");
			var colname = cols[DESC_COLNAME_IDX];
			var units = cols[DESC_UNITS_IDX];
			var name = cols[DESC_NAME_IDX];
			var datatype = cols[DESC_DTYPE_IDX];
			this.model.addDesc(name,units,datatype,colname);
		}
		this.model.addDesc("Lattitude","rad","float",null);
		this.model.addDesc("Longitude","rad","float",null);
		hideLoading();
		enableAll();
	}
}

function plot_stateChanged(response){
	if(response.readyState == 4 && response.status==200){
        var responseText = response.responseText;
        if (responseText.length < 1){
        	window.alert("No data available");
        }
		var coords = responseText.split("\n");
		for(i in coords){
			var dataArray = coords[i].split(",")
			if(dataArray.length > 1){
				this.model.addDataRow(dataArray); // add raw data
			}
		}
		this.model.createGPSData();
		var gpsData = this.model.getGPSData();
		var interval = this.options.interval;
		if(!this.isPlayback){
			plotAllData(gpsData);
		} else {
			intervalHandler = setInterval(function(){liveViewUpdate()},interval);
			hideLoading();
			hideMenu();
		}
		if(this.isLive){
			// setup interval to poll database
			liveDataUpdateHandler = setInterval(function(){liveDataUpdate()},interval);
			// setup interval to update view
			liveViewUpdateHandler = setInterval(function(){liveViewUpdate()},interval);
		}
	}
	
}

function playback_clicked(){
    this.isPlayback = true;
    this.plot_clicked();
}


function playback_stateChanged(response){
	if(response.readyState == 4 && response.status==200){
        var responseText = response.responseText;
		var coords = responseText.split("\n");
        gpsdata = createGPSData(coords);
        
        intervalHandler = setInterval(function(){intervalTick()},INTERVAL_DELAY);
    }
}



function intervalTick()
{
    var nextLocation = gpsdata.getNextPlayback();
    // set the first element in array as center
    // only do once per playback
    if(gpsdata.currentPlaybackIndex() == 1){
       setCenter(nextLocation);
    }

    if(nextLocation != null){    
        addMarker(nextLocation);
    } else {
        clearInterval(intervalHandler);
    }
}
// new vehicle selected in drop down, get the description and min/max time for it
function vehicle_changed(){
	var selectedVID = $("#vehicle_select option:selected").val();
	disableAll();

	if(selectedVID != INVALID_OPTION){
		showLoading();
	    this.model.selectedVID = selectedVID;
	    getDescriptionData(selectedVID);
	    getRange(selectedVID);
   }
}
// get the min and max time for the data for this vehicle
function getRange(vid){
	var request = new XMLHttpRequest();
	request.onreadystatechange=function(){
		getRange_stateChanged(request);
	}
	request.open("GET","api/getRange.php?vid="+vid,true);
	request.send();
}

function getRange_stateChanged(response){
	if(response.readyState == 4 && response.status==200){
        var responseText = response.responseText;
		var values = responseText.split(",");
		if(values.length > 1){
        	$('#startDate_picker').val(floatToDateTimeString(values[0]));
        	$('#endDate_picker').val(floatToDateTimeString(values[1]));
       	}
    }
}
// convert the epoch ms to date string
function floatToDateTimeString(f){
	var date = new Date(parseInt(f,10));
	// add leading 9 to single digit numbers
	var day = date.getDate();
	if(day < 10) day = "0"+day.toString();
	
	var month = date.getMonth()+1; // for some reason month is one less
	if(month < 10) month = "0"+month.toString();
	
	var year = date.getFullYear();
	
	var mins = date.getMinutes();
	if(mins < 10) mins = "0"+mins.toString();
	
	var hours = date.getHours();
	if(hours < 10) hours = "0"+hours.toString();
	
	var seconds = date.getSeconds();
	if(seconds < 10) seconds = "0"+seconds.toString();
	
	return day+"/"+month+"/"+year+" "+hours+":"+mins+":"+seconds;
}

function expandDiv(){
	$('#hidden_div').css('display','inline');
}

function initialise() {
	// set buttons to disabled
	disableAll();
	
	resize_function();

	initialiseMap();
	
	$(window).resize(function(){
		resize_function();
	});

	drawCharts(50,8,20);
	
	$(function () {
		$('#gauges_div').dblclick(function () {
		$(this).toggleMenu('#menu_div');
		});
	});
	
	$(function() {
    $('#vehicle_select').change(function() {
        vehicle_changed();
    	});    
	});
	
	$(function(){
		$('#menu_adv').click(function(){
			expandDiv();
		});
	});
	
	connect_clicked();
}

function hideMenu_clicked(){
	toggleMenu();
}
function hideMenu_complete(){
	if(!$("#menu_div").is(":visible")){
		$("#map_canvas").width($(window).width()); //bigger
	}
}

function resize_function(){

	var width = $(window).width();
	var height = $(window).height();


	if($("#menu_div").is(":visible")){
		$("#map_canvas").width(width);
	} else {
		$("#map_canvas").width(width);
	}
	$("#map_canvas").height(height*MAP_HEIGHT);
	$("#gauges_div").height(height - $("#map_canvas").height() );

}
// download csv file
function download_clicked(){
    var selectedVID = $("#vehicle_select option:selected").val();
    setOptions();

	var maxPoints = this.options.maxPoints;
	
	var startString = $('#startDate_picker').val();
	var endString = $('#endDate_picker').val();
	// convert to date epoch ms
	var startParts = startString.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/);
	if(startParts != null){
		var startDate = new Date(+startParts[3], startParts[2]-1, +startParts[1], +startParts[4], +startParts[5]);
		var start = startDate.valueOf();
	}
	
	var endParts = endString.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/);
	if(endParts != null){
		var endDate = Date.UTC(+endParts[3], endParts[2]-1, +endParts[1], +endParts[4], +endParts[5]);
		var end = endDate.valueOf();
	}
	var request;
	if(endParts == null || startParts == null){
		request = "api/downloadData.php?vid="+selectedVID+"&zone=56&convert=true&max="+maxPoints;
	} else {
		request = "api/downloadData.php?vid="+selectedVID+"&zone=56&convert=true&max="+maxPoints+"&start="+start+"&end="+end;
	}

	var host = window.location.host;
	var pathname = window.location.pathname;
	var url = host + pathname+request;
	window.open("http://"+url);
}


