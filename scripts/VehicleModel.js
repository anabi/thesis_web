// class VehicleModel
var UNASSIGNED = -1;
function VehicleModel(name,id,typeid,table){
	this.name = name;
	this.id = id;
	this.typeid = typeid;
	this.table = table;
	
	this.descriptions = [];	 // array of DescriptionModel objects
	this.data = []; // array of DataRow objects
	
	this.gps = new GPSData();
	
	this.time_index = UNASSIGNED;
	this.speed_index = UNASSIGNED;
	this.engTemp_index = UNASSIGNED;
	this.fuel_index = UNASSIGNED;
	
	this.indexesInit = false;
}

VehicleModel.prototype.getSpeed = function(){
	this.setIndexes();
	if(this.speed_index === UNASSIGNED){
		return UNASSIGNED;
	}
	return this.data[this.data.length-1].getField(this.speed_index);
}

VehicleModel.prototype.getEngTemp = function(){
	this.setIndexes();
	if(this.engTemp_index === UNASSIGNED){
		return UNASSIGNED;
	}
	return this.data[this.data.length-1].getField(this.engTemp_index);
}

VehicleModel.prototype.getFuel = function(){
	this.setIndexes();
	if(this.fuel_index === UNASSIGNED){
		return UNASSIGNED;
	}	
	return this.data[this.data.length-1].getField(this.fuel_index);
}

VehicleModel.prototype.getLastTime = function(){
	this.setIndexes();
	if(this.time_index === UNASSIGNED){
		return UNASSIGNED;
	}
	var lastIndex = this.data.length - 1;
	return this.data[lastIndex].getField(this.time_index);
}
// return the descriptions array
VehicleModel.prototype.getInfo = function(){
	if(this.descriptions.length == 0){
		return null;
	} else {
		return this.descriptions;
	}
}

VehicleModel.prototype.addDesc = function(name,units,datatype,colname){
	this.descriptions.push(new DescriptionModel(name,units,datatype,colname));
}

VehicleModel.prototype.setIndexes = function(descriptions){
	// get index of first datetime field
	if(this.indexesInit === false){
		for(var i = 0; i < this.descriptions.length; i++){
			var d = this.descriptions[i];
			if (this.time_index === UNASSIGNED && 
				d.name.toUpperCase().indexOf("TIME") > -1 ){
				this.time_index = i;
			} else if (this.speed_index === UNASSIGNED &&
				d.units.toUpperCase().indexOf("M/S") > -1){
					this.speed_index = i;
			} else if (this.engTemp_index === UNASSIGNED &&
				d.name.toUpperCase().indexOf("ENGINE") > -1){
					this.engTemp_index = i;
			} else if (this.fuel_index === UNASSIGNED &&
				d.name.toUpperCase().indexOf("FUEL") > -1){
					this.fuel_index = i;		
			}
			
		}
		this.indexesInit = true;
	}
}

VehicleModel.prototype.addDataRow = function(array){
	this.data.push(new DataRow(array));
}

VehicleModel.prototype.getGPSData = function(){
	return this.gps;
}

VehicleModel.prototype.createInfo = function(index){
	var info="";
	var name,units,data;
	var length = this.data[0].getLength();
	info += "<p>"
	for(var i = 0; i < length; i++){
		name = this.descriptions[i].name;
		units = this.descriptions[i].units;
		data = this.data[index].getField(i);
		if(i == this.time_index){
			var date = new Date(parseInt(data,10));
			//data = date.toString();
			data = date.toLocaleTimeString() +" "+date.toLocaleDateString();
		}
		info += name+": "+data+" "+units+"<br/>";
	}
	info += "</p>";
	
	return info;
}

VehicleModel.prototype.createGPSDataPoint = function(dataIndex){
	lat = this.data[dataIndex].getLat();
	lng = this.data[dataIndex].getLong();
	var pointInfo = this.createInfo(dataIndex);
	this.gps.add(new google.maps.LatLng(lat,lng),pointInfo);
}

VehicleModel.prototype.createGPSData = function(){

	for(i in this.data){
		this.createGPSDataPoint(i);
	}
}