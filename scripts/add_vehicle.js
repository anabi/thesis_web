var INVALID_OPTION = -1;

function initialise(){
	disableButton('#add_button');
	// send ajax request to get types of vehicles available
	var request = new XMLHttpRequest();
	request.onreadystatechange = function(){
		vehicleList_stateChanged(request);	
	}	
	request.open("GET","getTypes.php/?submit=vtypes")
	request.send();
}

function vehicleList_stateChanged(response){
    if(response.readyState == 4 && response.status==200){
		$('#type_select').find('option').remove().end(); // clear select element
		// insert the "Select Type" option
		$("#type_select").append("<option value='" + INVALID_OPTION + "'>" + "Select Type" + "</option>");
		var rows = response.responseText.split("\n");
		//insert all received options
		for(var i = 0; i < rows.length; i++){
			var values = rows[i].split(",");
			var id = values[0];
			var name = values[1];
			$("#type_select").append("<option value='" + id + "'>" + name + "</option>");
		}	
	}	
}

// enable add button once option is selected
function selectChanged(){
	var selectedTID = $("#type_select option:selected").val();

	if(selectedTID != INVALID_OPTION){
		enableButton('#add_button');
   } else {
   		disableButton('#add_button');
   }	
	
}
