// stores description information
function DescriptionModel(name,units,datatype,colname){
	this.colname = colname;
	this.name = name;
	this.units = units;
	this.datatype = datatype;
}
// main model
function Model(){
	this.vehicleList = [];
	this.selectedVID = 0;
}

Model.prototype.getLastTime = function(){
	var idx = this.getSelectedVehicleIndex();
	return this.vehicleList[idx].getLastTime();
}

// has the description for this vid been initialised??
Model.prototype.descInitialised = function(vid){
	var index = this.getVehicleIndex(vid);
	if(this.vehicleList[index].getInfo() == null){
		return false;
	} else {
		return true;
	}
}

Model.prototype.getVehicleIndex = function(vid){
	for(var i = 0; i < this.vehicleList.length; i++){
		if(this.vehicleList[i].id == vid){
			return i;
		}
	}
}

Model.prototype.getGPSData = function(){
	var selectedIdx = this.getSelectedVehicleIndex();
	return this.vehicleList[selectedIdx].getGPSData();
}


Model.prototype.createGPSData_avg = function(numPoints){
	var selectedIdx = this.getSelectedVehicleIndex();
	this.vehicleList[selectedIdx].createGPSData_avg(numPoints);
}

Model.prototype.createGPSData = function(){
	var selectedIdx = this.getSelectedVehicleIndex();
	this.vehicleList[selectedIdx].createGPSData();
}

Model.prototype.createLastGPSData = function(){
	var selectedIdx = this.getSelectedVehicleIndex();
	var vehicle = this.vehicleList[selectedIdx];
	var idx = vehicle.data.length-1; 
	vehicle.createGPSDataPoint(idx);
}


Model.prototype.getSelectedVehicleIndex = function(){
	return this.getVehicleIndex(this.selectedVID);
}

Model.prototype.getSelectedVehicle = function(){
	return this.vehicleList[this.getSelectedVehicleIndex()];
}

Model.prototype.addDataRow = function(array){
	var index = this.getSelectedVehicleIndex();
	this.vehicleList[index].addDataRow(array);
}

Model.prototype.addDesc = function(name,units,datatype,colname){
	var selectedIdx = this.getSelectedVehicleIndex();
	this.vehicleList[selectedIdx].addDesc(name,units,datatype,colname);
}

Model.prototype.addVehicle = function(name, id, typeid, table){
	this.vehicleList.push(new VehicleModel(name,id,typeid,table));
}

Model.prototype.getVehicleList = function() {
	if(this.vehicleList.length == 0)
		return null;
	
	return this.vehicleList;
}

function Options(){
	this.maxPoints = MAX_POINTS;
	this.interval = INTERVAL_DELAY;
}
